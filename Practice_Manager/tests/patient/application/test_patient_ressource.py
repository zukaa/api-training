import unittest
from unittest.mock import Mock
from flask.testing import FlaskClient
import json


from app import create_app
from patient.application.patient_service import PatientService
from patient.domain.patient import Patient

class TestPatientResource(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config['TESTING'] = True
        self.client: FlaskClient = self.app.test_client()

        self.mock_service = Mock(spec=PatientService)
        self.app.config['patient_service'] = self.mock_service
 
    def test_create_patient_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_service.create_patient.return_value = patient_data

        response = self.client.post('/patients', json=patient_data)

        self.assertEqual(response.status_code, 400)
        print(response.json)
        self.assertEqual(response.json, patient_data)
        self.mock_service.create_patient.assert_called_once()

    def test_search_patients_by_social_security_number(self):
        social_security_number = '1 36 57 637 278 977'
        expected_patient_data = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': social_security_number
        }

        self.mock_service.search_patient_by_social_security_number.return_value = expected_patient_data

        response = self.client.get(f'/patients/search?social_security_number={social_security_number}')

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, expected_patient_data)
        self.mock_service.search_patient_by_social_security_number.assert_called_once_with(social_security_number)

    def test_update_patient(self):
        patient_id = 1
        new_patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789123457'
        }

        self.mock_service.update_patient.return_value = {'id': patient_id, **new_patient_data}

        response = self.client.put(f'/patients/{patient_id}', json=new_patient_data)
        
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {'id': patient_id, **new_patient_data})
        self.mock_service.update_patient.assert_called_once_with(patient_id, Patient(**new_patient_data))

    def test_delete_patient(self):
        patient_id = 1
        new_patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789123457'
        }
        self.mock_service.delete_patient.return_value = {'id': patient_id, 'message': 'Patient deleted successfully'}

        response = self.client.delete(f'/patients/{patient_id}')

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {'message': 'Patient deleted successfully'})
        self.mock_service.update_patient.assert_called_once_with(patient_id, new_patient_data)



    def test_get_patient_by_id(self):
        patient_id = 1
        expected_patient_data = {
            'id': patient_id,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        mock_patient_service = Mock(spec=PatientService)
        mock_patient_service.get_patient_by_id.return_value = expected_patient_data
        self.app.config['patient_service'] = mock_patient_service

        response = self.app.test_client().get(f'/patients/{patient_id}')
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(response.status_code, 400)
        self.assertEqual(data, expected_patient_data)
        mock_patient_service.get_patient_by_id.assert_called_once_with(patient_id)

if __name__ == '__main__':
    unittest.main()