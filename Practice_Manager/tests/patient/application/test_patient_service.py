import unittest
import unittest.mock as mock

from patient.application.patient_service import PatientService


class TestPatientService(unittest.TestCase):
    def setUp(self):
        self.mock_repository = mock.Mock()
        self.patient_service = PatientService(self.mock_repository)

    def test_create_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_repository.add_patient.return_value = {'id': 1, **patient_data}

        new_patient = self.patient_service.create_patient(patient_data)

        self.assertIsNotNone(new_patient)
        self.assertEqual(new_patient['id'], 1)
        self.assertEqual(new_patient['first_name'], 'John')
        self.assertEqual(new_patient['last_name'], 'Doe')
        self.assertEqual(new_patient['date_of_birth'], '1980-01-01')
        self.assertEqual(new_patient['social_security_number'], '123456789012345')

        self.mock_repository.add_patient.assert_called_once_with(patient_data)

    def create_patient_with_missing_value_raises_value_error(self, missing_field):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        del patient_data[missing_field]

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

    def test_create_patient_with_missing_first_name(self):
        self.create_patient_with_missing_value_raises_value_error('first_name')

    def test_create_patient_with_missing_last_name(self):
        self.create_patient_with_missing_value_raises_value_error('last_name')

    def test_create_patient_with_missing_date_of_birth(self):
        self.create_patient_with_missing_value_raises_value_error('date_of_birth')

    def test_create_patient_with_missing_social_security_number(self):
        self.create_patient_with_missing_value_raises_value_error('social_security_number')

    def test_create_patient_with_invalid_birth_date(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-13-01',
            'social_security_number': '123456789012345'
        }

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

        self.mock_repository.add_patient.assert_not_called()



    def test_search_patient_by_social_security_number(self):
        social_security_number = '1 36 57 637 278 977'
        expected_patient_data = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': social_security_number
        }

        self.mock_repository.get_patient_by_social_security_number.return_value = expected_patient_data

        found_patient = self.patient_service.search_patient_by_social_security_number(social_security_number)

        self.assertIsNotNone(found_patient)
        self.assertEqual(found_patient, expected_patient_data)

        self.mock_repository.get_patient_by_social_security_number.assert_called_once_with(social_security_number)

    def test_search_patient_by_missing_social_security_number_raises_value_error(self):
        with self.assertRaises(ValueError):
            self.patient_service.search_patient_by_social_security_number('')
    
    def test_search_patient_by_name(self):
        name = 'John Doe'
        expected_patient_data = [
            {
                'id': 1,
                'first_name': 'John',
                'last_name': 'Doe',
                'date_of_birth': '1980-01-01',
                'social_security_number': '1 36 57 637 278 977'
            },
        ]

        self.mock_repository.get_patients_by_name.return_value = expected_patient_data

        found_patients = self.patient_service.search_patient_by_name(name)

        self.assertIsNotNone(found_patients)
        self.assertEqual(found_patients, expected_patient_data)

        self.mock_repository.get_patients_by_name.assert_called_once_with(name)

    def test_search_patient_by_missing_name_raises_value_error(self):
        with self.assertRaises(ValueError):
            self.patient_service.search_patient_by_name('')
    
    def test_search_patient_by_surname(self):
        surname = 'Doe'
        expected_patient_data = [
            {
                'id': 1,
                'first_name': 'John',
                'last_name': 'Doe',
                'date_of_birth': '1980-01-01',
                'social_security_number': '1 36 57 637 278 977'
            },
        ]

        self.mock_repository.get_patients_by_surname.return_value = expected_patient_data

        found_patients = self.patient_service.search_patient_by_surname(surname)

        self.assertIsNotNone(found_patients)
        self.assertEqual(found_patients, expected_patient_data)

        self.mock_repository.get_patients_by_surname.assert_called_once_with(surname)

    def test_search_patient_by_missing_surname_raises_value_error(self):
        with self.assertRaises(ValueError):
            self.patient_service.search_patient_by_surname('')


    def test_update_patient(self):
        old_ssn = '123456789123456'
        new_patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789123457'
        }

        existing_patient = mock.MagicMock(social_security_number=old_ssn)
        
        # Configurez le mock pour que la recherche du nouveau numéro de sécurité sociale retourne None,
        # indiquant qu'aucun autre patient n'est associé à ce numéro.
        self.mock_repository.get_patient_by_social_security_number.side_effect = lambda ssn: existing_patient if ssn == old_ssn else None

        self.patient_service.update_patient(old_ssn, new_patient_data)

        # Assurez-vous que la méthode d'update_patient du repository est appelée avec le bon patient.
        self.mock_repository.update_patient.assert_called_once_with(existing_patient)

    def test_delete_patient(self):
        patient_name = 'John Doe'

        existing_patient = mock.MagicMock(name=patient_name)
        self.mock_repository.get_patient_by_name.return_value = existing_patient

        self.patient_service.delete_patient(patient_name)

        self.mock_repository.delete_patient.assert_called_once_with(existing_patient)

    
    def test_get_patient_by_id(self):
        patient_id = 1
        expected_patient_data = {
            'id': patient_id,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_repository.get_patient_by_id.return_value = expected_patient_data

        fetched_patient = self.patient_service.get_patient_by_id(patient_id)

        self.assertIsNotNone(fetched_patient)
        self.assertEqual(fetched_patient, expected_patient_data)

        self.mock_repository.get_patient_by_id.assert_called_once_with(patient_id)

if __name__ == '__main__':
    unittest.main() 


