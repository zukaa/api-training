import unittest
from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity
from patient.application.patient_service import PatientService

class PatientRepository:
    def __init__(self):
        self.patients = []
        self.counter = 1


class TestPatientRepository(unittest.TestCase):
    def setUp(self):
        self.repository = PatientRepository()

    def test_add_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        self.assertEqual(self.repository.get_patient(new_patient.id), PatientEntity(**{'id': 1, **patient_data}))

    def test_get_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        retrieved_patient = self.repository.get_patient(new_patient.id)

        self.assertEqual(retrieved_patient, PatientEntity(**{'id': 1, **patient_data}))


    def search_patient_by_social_security_number(self, social_security_number):
        found_patient = self.get_patient_by_social_security_number(social_security_number)
        return found_patient

    def search_patient_by_name(self, name):
        found_patients = self.get_patients_by_name(name)
        return found_patients

    def search_patient_by_surname(self, surname):
        found_patients = self.get_patients_by_surname(surname)
        return found_patients

    def update_patient(self, patient):
        for i, existing_patient in enumerate(self.patients):
            if existing_patient['id'] == patient['id']:
                self.patients[i] = patient.to_dict()
                return patient.to_dict()
        raise ValueError(f'No patient found with ID: {patient["id"]}')
    

    def delete_patient(self, patient_id):
        for i, patient in enumerate(self.patients):
            if patient['id'] == patient_id:
                del self.patients[i]
                return
        raise ValueError(f'No patient found with ID: {patient_id}')

    def get_patient_by_id(self, patient_id):
        for patient in self.patients:
            if patient['id'] == patient_id:
                return patient
        return None