from datetime import datetime

from patient.domain.patient import Patient


class PatientService:
    def __init__(self, patient_repository):
        self.patient_repository = patient_repository

    def create_patient(self, patient_data):
        required_fields = ['first_name', 'last_name', 'date_of_birth', 'social_security_number']
        if any(not patient_data.get(field) for field in required_fields):
            raise ValueError('Patient first name is required')
        if patient_data['date_of_birth'] != datetime.strptime(patient_data['date_of_birth'], '%Y-%m-%d') \
                .strftime('%Y-%m-%d'):
            raise ValueError('Patient date of birth is invalid')
        # patient = Patient(**patient_data)
        created_patient = self.patient_repository.add_patient(patient_data)
        return created_patient

    def search_patient_by_social_security_number(self, social_security_number):
        if not social_security_number:
            raise ValueError('Social Security Number is required for searching a patient.')
        
        found_patient = self.patient_repository.get_patient_by_social_security_number(social_security_number)

        if not found_patient:
            raise ValueError(f'Patient with Social Security Number {social_security_number} not found.')

        return found_patient

    def search_patient_by_name(self, name):
        if not name:
            raise ValueError('Name is required for searching a patient.')

        found_patients = self.patient_repository.get_patients_by_name(name)

        if not found_patients:
            raise ValueError(f'No patients found with the name {name}.')

        return found_patients
    
    def search_patient_by_surname(self, surname):
        if not surname:
            raise ValueError('Surname is required for searching a patient.')

        found_patients = self.patient_repository.get_patients_by_surname(surname)

        if not found_patients:
            raise ValueError(f'No patients found with the surname {surname}.')

        return found_patients

    def update_patient(self, old_ssn, new_patient_data):
        patient = self.patient_repository.get_patient_by_social_security_number(old_ssn)

        if patient:
            
            new_ssn = new_patient_data.get('social_security_number')
            if new_ssn and self.patient_repository.get_patient_by_social_security_number(new_ssn):
                raise ValueError(f"The social security number {new_ssn} is already assigned to another patient.")

            patient.social_security_number = new_patient_data.get('social_security_number', patient.social_security_number)

            
            self.patient_repository.update_patient(patient)
        else:
            raise ValueError(f"No patient found with social security number: {old_ssn}")
        
    def delete_patient(self, patient_name):
        patient = self.patient_repository.get_patient_by_name(patient_name)

        if patient:
            
            self.patient_repository.delete_patient(patient)
        else:
            raise ValueError(f"No patient found with name: {patient_name}")


    def get_patient_by_id(self, patient_id):
        if not patient_id:
            raise ValueError('Patient ID is required for fetching patient information.')

        found_patient = self.patient_repository.get_patient_by_id(patient_id)

        if not found_patient:
            raise ValueError(f'Patient with ID {patient_id} not found.')

        return found_patient


 