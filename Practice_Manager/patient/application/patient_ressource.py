from flask import request, jsonify, Blueprint
from flask import current_app as app


from patient.domain.patient import Patient

patient_bp = Blueprint('patient', __name__)
 

@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/search', methods=['GET'])
def search_patients():
    try:
        social_security_number = request.args.get('social_security_number')
        name = request.args.get('name')
        surname = request.args.get('surname')

        patient_service = app.config['patient_service']
        
        if social_security_number:
            found_patients = patient_service.search_patient_by_social_security_number(social_security_number)
        elif name:
            found_patients = patient_service.search_patient_by_name(name)
        elif surname:
            found_patients = patient_service.search_patient_by_surname(surname)
        else:
            raise ValueError('At least one search parameter is required (social_security_number, name, or surname).')

        return jsonify(found_patients), 200
    except ValueError as e:
        return jsonify({'error': str(e)}), 400

 
    
@patient_bp.route('/patients/<int:patient_id>', methods=['PUT'])
def update_patient(patient_id):
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        updated_patient = patient_service.update_patient(patient_id, Patient(**patient_data))
        return jsonify(Patient(**updated_patient).to_dict()), 200
    except ValueError as e:
        return jsonify({'error': str(e)}), 400


@patient_bp.route('/patients/<int:patient_id>', methods=['DELETE'])
def delete_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient_service.delete_patient(patient_id)
        return jsonify({'message': 'Patient deleted successfully'}), 200
    except ValueError as e:
        return jsonify({'error': str(e)}), 400



@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def get_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        fetched_patient = patient_service.get_patient_by_id(patient_id)

        return jsonify(fetched_patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400